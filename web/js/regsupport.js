var validateEmail = function(email) {
	var re=/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}
$(document).ready(function(){
  $('.user-text-reg').focusout(function(event) {
    var html="<ul class='ul-login' style='list-style-type:none'>"+
                "<li>El campo Usuario no puede estar vacio</li>"+
                "<li>El usuario debe contener menos de 10 caracteres</li>"+
              "</ul>";
    $('.user-text-reg').popover({
      placement:	'top',
      title: "",
      html:true,
      content: html,
      trigger: 'manual'
    });
    if($('.user-text-reg').val()==="" || $('.user-text-reg').val().trim().length>10){
      $(this).popover('show');
      setTimeout(function(){
        $('.user-text-reg').popover('hide');
      },4000);
    }
  });
  $('.user-text-reg').click(function(event) {
    $('.user-text-reg').popover('hide');
  });
  $('.password-text-reg').focusout(function(event) {
    var html="<ul class='ul-login' style='list-style-type:none'>"+
                "<li>El campo Password no puede estar vacio</li>"+
                "<li>La contraseña debe contener menos de 10 caracteres</li>"+
              "</ul>";
    $('.password-text-reg').popover({
      placement:	'bottom',
      title: "",
      html:true,
      content: html,
      trigger: 'manual'
    });
    if($('.password-text-reg').val()==="" || $('.password-text-reg').val().trim().length>10){
      $(this).popover('show');
      setTimeout(function(){
        $('.password-text-reg').popover('hide');
      },4000);
    }
  });
  $('.password-text-reg').click(function(event) {
    $('.password-text-reg').popover('hide');
  });
  $('.rep-password-text-reg').focusout(function(event) {
    var html = "<ul class='ul-login' style='list-style-type:none'>"+
                "<li>El campo repetir contraseña no puede estar vacio</li>"+
                "<li>Las contraseñas deben coincidir</li>"+
               "</ul>";
    $('.rep-password-text-reg').popover({
      placement:	'bottom',
      title: "",
      html:true,
      content: html,
      trigger: 'manual'
    });
    if($('.password-text-reg').val().trim()!==$('.rep-password-text-reg').val().trim()){
      $(this).popover('show');
      setTimeout(function(){
        $('.rep-password-text-reg').popover('hide');
      },4000);
    }
  });
	$('.rep-password-text-reg').click(function(event) {
		$('.rep-password-text-reg').popover('hide');
	});
  $('.email-text-reg').focusout(function(event) {
    var html = "<ul class='ul-login' style='list-style-type:none'>"+
                "<li>El campo email no puede estar vacio</li>"+
                "<li>Debe de ser un email válido</li>"+
               "</ul>";
    $('.email-text-reg').popover({
      placement:	'bottom',
      title: "",
      html:true,
      content: html,
      trigger: 'manual'
    });
		var email = $('.email-text-reg').val();
    if($('.email-text-reg').val()==="" || !validateEmail($('.email-text-reg').val())){
      $(this).popover('show');
      setTimeout(function(){
        $('email-text-reg').popover('hide');
      },4000);
    }
		$('.email-text-reg').click(function(event) {
			$('.email-text-reg').popover('hide');
		});
  });
	var enterUser = false;
	var enterPass = false;
	var enterRepPass = false;
	var enterEmail = false;
	$('.user-text-reg').bind('input', function(){
		if($('.user-text-reg').val()==="" || $('.user-text-reg').val().trim().length>10){
			enterUser = false;
		}else{
			enterUser = true;
		}
		if(enterUser && enterPass && enterRepPass && enterEmail){
			$('.reg-support').removeClass('disabled');
			$('.reg-support').removeAttr('disabled');
		}else{
			$('.reg-support').addClass('disabled');
			$('.reg-support').prop('disabled',true);
		}
	});
	$('.password-text-reg').bind('input', function(){
		if($('.password-text-reg').val()==="" || $('.password-text-reg').val().length>10){
console.log(enterPass);
			enterPass = false;
		}else{
			console.log(enterPass);
			enterPass = true;
		}
		if(enterUser && enterPass && enterRepPass && enterEmail){
			$('.reg-support').removeClass('disabled');
			$('.reg-support').removeAttr('disabled');
		}else{
			$('.reg-support').addClass('disabled');
			$('.reg-support').prop('disabled',true);
		}
	});
	$('.rep-password-text-reg').bind('input', function(){
		if($('.password-text-reg').val().trim()!==$('.rep-password-text-reg').val().trim()){
console.log(enterRepPass);
			enterRepPass = false;
		}else{
			console.log(enterRepPass);
			enterRepPass = true;
		}
		if(enterUser && enterPass && enterRepPass && enterEmail){
			$('.reg-support').removeClass('disabled');
			$('.reg-support').removeAttr('disabled');
		}else{
			$('.reg-support').addClass('disabled');
			$('.reg-support').prop('disabled',true);
		}
	});
	$('.email-text-reg').bind('input', function(){
		if($('.email-text-reg').val()==="" || !validateEmail($('.email-text-reg').val())){
			console.log(enterEmail);
			enterEmail = false;
		}else{
			console.log(enterEmail);
			enterEmail = true;
		}
		if(enterUser && enterPass && enterRepPass && enterEmail){
			$('.reg-support').removeClass('disabled');
			$('.reg-support').removeAttr('disabled');
		}else{
			$('.reg-support').addClass('disabled');
			$('.reg-support').prop('disabled',true);
		}
	});

});
