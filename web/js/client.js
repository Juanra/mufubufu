(function($) {
	$.get = function(key) {
		key = key.replace(/[\[]/, '\\[');
		key = key.replace(/[\]]/, '\\]');
		var pattern = "[\\?&]" + key + "=([^&#]*)";
		var regex = new RegExp(pattern);
		var url = unescape(window.location.href);
		var results = regex.exec(url);
		if (results === null) {
			return null;
		} else {
			return results[1];
		}
	}
})(jQuery);

var param = $.get('group');
var socket;
if(param === 'user') {
	socket=io('localhost:3000/?member=user');
}else if(param === 'staff') {
	socket=io('localhost:3000/?member=staff');
}
var myCookies = document.cookie;
var expreg = /(usrid=)(\d+)/;
var id = myCookies.match(expreg);
socket.emit('my_id', { id: id[2], group: param });
