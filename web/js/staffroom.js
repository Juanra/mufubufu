$(document).ready(function(){
//  var id_user = 0;
  var expreg = /(usrid=)(\d)/;
  var id = myCookies.match(expreg);
  $( ".input-chat" ).prop( "disabled", true );
  socket.on('new_message', function(data) {
    var message = data.message;
    var date = data.date_message;
    $.ajax({
      url: '/web/index.php?ctl=userroom',
      method: 'POST',
      context: document.body,
      data: {id: data.id_db}
    }).done(function(data) {
      data = JSON.parse(data);
      $( ".input-chat" ).prop( "disabled", false );
      console.log("No se sabe:");
      console.log(data.id);
      console.log($('.conversation').length);
      if($('.conversation[data-id="'+data.id+'"]').length !== 0) {
        //Si la conversacion existe cambiar la hora y el mensaje a mostrar en recentConversations
        //Añadir el mensaje en la conversacion.
        $('.conversation[data-id="'+data.id+'"]').find('.con-message-label').html(message);
        $('.conversation[data-id="'+data.id+'"]').find('.date').html(date);
        console.log("hay mas de una conversacion:");
        console.log($('.content-messages-chat').attr('data-id'));
        console.log(data.id);
        $('.selected').attr('data-id');
        if($('.content-messages-chat').attr('data-id') == data.id){
            $('.content-messages-chat').append('<div class="col-xs-12">'+
                                                '<span class="speech-other">'+
                                                message+
                                                '<div class="timestamp pull-right">'+
                                                date+
                                                '<span class="material-icons chat-check">'+
                                                'done_all'+
                                                '</span>'+
                                                '</div>'+
                                                '</span>'+
                                                '</div>');
        }
        fnChangeConversations();
      }else {

        $('.content-recent-conversations').prepend('<div class="conversation" data-id="'+data.id+'">'+
                                                      '<div class="con-left-img"></div>'+
                                                      '<div class="con-name-message">'+
                                                        '<span class="con-name-label">'+
                                                          data.username+
                                                        '</span></br>'+
                                                        '<span class="con-message-label">'+
                                                          message+
                                                        '</span>'+
                                                      '</div>'+
                                                      '<div class="con-right-date">'+
                                                        '<span class="date">'+date+'</span>'+
                                                        '<span class="material-icons rm-chat" id-rm="'+data.id+'">clear</span>'+
                                                      '</div>'+
                                                    '</div>');
        $('.content-messages-chat').attr('data-id',data.id);
        $('.content-messages-chat').append('<div class="col-xs-12">'+
                                              '<span class="speech-other">'+
                                                message+
                                                '<div class="timestamp pull-right">'+
                                                  date+
                                                  '<span class="material-icons chat-check">'+
                                                    'done_all'+
                                                  '</span>'+
                                                '</div>'+
                                              '</span>'+
                                            '</div>');
      }
      $('.colum-chat').scrollTop($('.content-messages-chat').height());
    });
  });

/*  $(document.body).on('click', '.conversation', function(event) {
    id_user = $(this).attr('data-id');
    console.log({ id: socket.id, to: id_user, message: 'respuesta', group: 'staff' });
    socket.emit('answer', { id: socket.id, to: id_user, message: 'respuesta', group: 'staff' });
  }); */

  $('.input-chat').html("");
  $('.input-chat').focus();
  var id_to;
  $(document.body).on('keydown', '.input-chat', function(event) {
    if(event.keyCode==13) {
      event.preventDefault();
      if($('.show-indication > span').html() !== ""){
        $('.show-indication').addClass('hidden');
      }
      if($(this).val()!='' && $(this).val()!=null && $(this).val()!=undefined) {
        var message = $('.input-chat').val();
        $(this).val("");
        var f = new Date();
        var date = formatDate(f);
        id_to = $('.content-messages-chat').attr('data-id');
        $('.content-messages-chat').append('<div class="col-xs-12">'+
                                            '<span class="speech">'+
                                              message+
                                              '<div class="timestamp pull-right">'+
                                                date+
                                                '<span class="material-icons chat-check">'+
                                                  'done_all'+
                                                '</span>'+
                                              '</div>'+
                                            '</span>'+
                                          '</div>');
        socket.emit('new_message', { id: socket.id, message: message, group: 'staff', id_db: id[2], date_message: date, to: id_to });
        $('.colum-chat').scrollTop($('.content-messages-chat').height());
      }
    }
  });
  $(document.body).on('click','.send-message', function(){
    var e = $.Event("keydown", {keyCode: 13});
    $('.input-chat').trigger(e);
  });

  $(document.body).on('click', '.rm-chat', function(event) {
    var id_chat = $(this).attr('id-rm');
    $.ajax({
      url: '/web/index.php?ctl=userroom&id='+id_chat+'&group=staff',
      method: 'DELETE',
      context: document.body,
    })
    .done(function(data) {
      $('.conversation[data-id="'+id_chat+'"]').html("");
      $('.content-messages-chat[data-id="'+id_chat+'"]').html("");
      $( ".input-chat" ).prop( "disabled", true );
      socket.emit('del-chat', {id: id[2], to: id_to});
    });
  });
  $(document.body).on('click', '.conversation', function(e){
    console.log($(this).attr('data-id'));
    //Juanra
    //Falta borrar todo $('.content-messages-chat')
    //Hacer peticion ajax para recibir conversacion del nuevo mensaje o el pulsado
    $('.content-messages-chat').remove();
  });
  function fnChangeConversations(){

  }
});

function formatDate(date){
  var minutes = date.getMinutes();
  var hours = date.getHours();
  if(minutes < 10){
    minutes = "0"+minutes;
  }
  if(hours < 10){
    hours = "0"+hours;
  }
  return hours+":"+minutes;
}
