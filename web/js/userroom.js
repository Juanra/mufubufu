var id_db = "";
$(document).ready(function(){
  var expreg = /(usrid=)(\d+)/;
  var id = myCookies.match(expreg);
  console.log(id[2]);
  $.ajax({
    url: '/app/getConversations.php',
    method: 'POST',
    context: document.body,
    data: {id: id[2]}
  })
  .done(function(data) {
    data = JSON.parse(data);
    if(data.data == "INTERNAL_ERROR"){
      //DO NOTHING
    }else{
      var lastMessage = data.data[data.data.length-1].content;
      var timeLastMessage = data.data[data.data.length-1].time_message;
      if($('.show-indication > span').html() !== ""){
        $('.show-indication').addClass('hidden');
      }
      $('.content-recent-conversations').prepend('<div class="conversation" data-id="'+data.id_db+'">'+
                                                  '<div class="con-left-img"></div>'+
                                                  '<div class="con-name-message">'+
                                                    '<span class="con-name-label">Support Member'+
                                                    '</span></br>'+
                                                    '<span class="con-message-label">'+
                                                      lastMessage+
                                                      '</span>'+
                                                  '</div>'+
                                                  '<div class="con-right-date">'+
                                                    '<span class="date">'+data.date_message+'</span>'+
                                                  '</div>'+
                                                 '</div>');
      for(var i = 0;i<data.data.length;i++){
        if(data.data[i].id_emissor_user != null){
          $('.content-messages-chat').append('<div class="col-xs-12">'+
                                              '<span class="speech">'+
                                                data.data[i].content+
                                                '<div class="timestamp pull-right">'+
                                                  data.data[i].time_message+
                                                  '<span class="material-icons chat-check">'+
                                                    'done_all'+
                                                  '</span>'+
                                                '</div>'+
                                              '</span>'+
                                             '</div>');
        }else{
          $('.content-messages-chat').append('<div class="col-xs-12">'+
          '<span class="speech-other">'+
          data.data[i].content+
          '<div class="timestamp pull-right">'+
          data.data[i].time_message+
          '<span class="material-icons chat-check">'+
          'done_all'+
          '</span>'+
          '</div>'+
          '</span>'+
          '</div>');
        }
        $('.colum-chat').scrollTop($('.content-messages-chat').height());
      }
    }
  });

  $('.input-chat').html("");
  $('.input-chat').focus();
  $(document.body).on('keydown', '.input-chat', function(event) {
    if(event.keyCode==13) {
      event.preventDefault();
      if($('.show-indication > span').html() !== ""){
        $('.show-indication').addClass('hidden');
      }
      if($(this).val()!='' && $(this).val()!=null && $(this).val()!=undefined) {
        var message = $('.input-chat').val();
        $(this).val("");
        var f = new Date();
        var date = formatDate(f);
        $('.content-messages-chat').append('<div class="col-xs-12">'+
                                            '<span class="speech">'+
                                              message+
                                              '<div class="timestamp pull-right">'+
                                                date+
                                                '<span class="material-icons chat-check">'+
                                                  'done_all'+
                                                '</span>'+
                                              '</div>'+
                                            '</span>'+
                                          '</div>');
        socket.emit('new_message', { id: socket.id, message: message, group: 'user', id_db: id[2], date_message: date });
        $('.colum-chat').scrollTop($('.content-messages-chat').height());
      }
    }
  });

  socket.on('new_message', function(data){
    if($('.conversation[data-id="'+data.id_db+'"]').length !== 0) {
      $('.conversation[data-id="'+data.id_db+'"]').find('.con-message-label').html(data.message);
      $('.conversation[data-id="'+data.id_db+'"]').find('.date').html(data.date_message);
      $('.content-messages-chat').append('<div class="col-xs-12">'+
                                            '<span class="speech-other">'+
                                              data.message+
                                              '<div class="timestamp pull-right">'+
                                                data.date_message+
                                                '<span class="material-icons chat-check">'+
                                                  'done_all'+
                                                '</span>'+
                                              '</div>'+
                                            '</span>'+
                                          '</div>');
    }else{
      $('.content-recent-conversations').prepend('<div class="conversation" data-id="'+data.id_db+'">'+
                                                    '<div class="con-left-img"></div>'+
                                                    '<div class="con-name-message">'+
                                                      '<span class="con-name-label">Support Member'+
                                                      '</span></br>'+
                                                      '<span class="con-message-label">'+
                                                        data.message+
                                                      '</span>'+
                                                    '</div>'+
                                                    '<div class="con-right-date">'+
                                                      '<span class="date">'+data.date_message+'</span>'+
                                                    '</div>'+
                                                  '</div>');
      $('.content-messages-chat').attr('data-id',data.id_db);
      $('.content-messages-chat').append('<div class="col-xs-12">'+
                                            '<span class="speech-other">'+
                                              data.message+
                                              '<div class="timestamp pull-right">'+
                                                data.date_message+
                                                '<span class="material-icons chat-check">'+
                                                  'done_all'+
                                                '</span>'+
                                              '</div>'+
                                            '</span>'+
                                          '</div>');
    }
    $('.colum-chat').scrollTop($('.content-messages-chat').height());
  });

  $(document.body).on('click','.send-message', function(){
    var e = $.Event("keydown", {keyCode: 13});
    $('.input-chat').trigger(e);
  });
  socket.on('del-chat', function(){
    $('.conversation').html("");
    $('.content-messages-chat').html("");
    $('.show-indication').removeClass('hidden');
    $( ".input-chat" ).prop( "disabled", true );
    $('.show-indication > span').html("Su consulta ha finalizado, esperamos haber solucionado tu problema. </br> Un saludo!");
  });
});

function formatDate(date){
  var minutes = date.getMinutes();
  var hours = date.getHours();
  if(minutes < 10){
    minutes = "0"+minutes;
  }
  if(hours < 10){
    hours = "0"+hours;
  }
  return hours+":"+minutes;
}
