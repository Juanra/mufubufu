var validateEmail = function(email) {
	var re=/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}
$(document).ready(function(){
  $('.offline-contact').click(function(){
    $('.content-of-chat').addClass('hidden');
    $('.content-of-offline-contact').removeClass('hidden');
  });
	var date = new Date();
	var hours = date.getHours();
	console.log(hours);
	if(hours<8 || hours>24){
		$('.btn-enter-room').addClass('back-red');
		$('.span-enter-room').html("Support Offline");
		$('.enter-room').css('display','none');
		$('.online-offline-text').html("En estos momentos nuestros técnicos no se encuentran en línea, puede contactar con nosotros con un mensaje que será respondido lo antes posible.");
	}
  $('.offline-contact-submit').click(function(e){
    var enterEmail=false;
    var enterSubject=false;
    var enterMessage=false;
    if ($('.email-text').val()==="" || !validateEmail($('.email-text').val())) {
      $('.error-email').html("Debes introducir un email");
    }else{
      enterEmail = true;
      $('.error-email').html("");
    }

    if($('.subject-text').val()==="" || $('.subject-text').val().length>150){
      $('.error-subject').html("Debes introducir un asunto");
    }else{
      enterSubject = true;
      $('.error-subject').html("");
    }

    if($('.textarea-message-text').val()==="" || $('.textarea-message-text').val().length>512){
      $('.error-message').html("Debes introducir un mensaje explicativo");
    }else{
      enterMessage = true;
      $('.error-message').html("");
    }
    if(enterEmail && enterSubject && enterMessage){
      return true;
    }else{
      e.preventDefault();
    }
  });
});
