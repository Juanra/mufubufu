var http = require('http');
var io = require('socket.io');
var mysql = require('mysql');
var _ = require('lodash');
var url = require('url');

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'dbsupport'
});

//Creacion del servidor

var app = http.createServer(function(req, res) {
  res.writeHead(200, { 'Content-Type': 'text/html' });
});

setInterval(function() {
  connection.query('SELECT 1');
}, 5000);

// connection.query('delete from `websocket`');

io = io.listen(app);
var users = [];
var staff = [];

io.on('connection', function(socket) {
  var user = { id: socket.id };
  if(socket.handshake.query.member === 'staff') {
    user.group = 'staff';
    staff.push(user);
    socket.emit('welcome', { message: 'Welcome', usersonline: users.length });
  }else {
    user.group = 'user';
    users.push(user);
    socket.emit('welcome', { message: 'Welcome', staffmembers: staff.length });
  }

  socket.on('new_message', function(data) {
    console.log(data);
    switch(data.group) {
      case 'user':
        for(var i = 0; i < staff.length; i++) {
          io.to(staff[i].id).emit('new_message', data);
        }
        if(!user.ticket) {
          connection.query("insert into `ticket` (id_user) values ("+user.id_db+")", function(err, rows, fields) {
            console.log(rows);
            connection.query("insert into `messages` (id_emissor_user, content, id_ticked, time_message) values ("+user.id_db+", '"+data.message+"', "+rows.insertId+", '"+data.date_message+"')", function(err, rows, fields) {
              console.log("ERROR DE USUARIO "+user.id_db+" SIN TICKET");
              console.log(err);
            });
            connection.query("select * from `ticket` where id_user = '"+user.id_db+"'", function(err, rows, fields){
              user.ticket_id = rows[0].id;
            });
            user.ticket = true;
          });
        }else {
          connection.query("insert into `messages` (id_emissor_user, content, id_ticked, time_message) values ("+user.id_db+", '"+data.message+"', "+user.ticket_id+", '"+data.date_message+"')", function(err, rows, fields) {
            console.log("ERROR DE USUARIO "+user.id_db+" CON TICKET");
            console.log(err);
          });
        }
        break;
      case 'staff':
        if(!staff.ticket){
          connection.query("select * from `ticket` where id_user = '"+data.to+"'", function(err, rows, fields){
            connection.query("update `ticket` set id_staff = "+data.id_db+" where id = "+rows[0].id, function(err, rows, fields){
            });
            connection.query("insert into `messages` (id_emissor_staff,content,id_ticked, time_message) value ("+data.id_db+", '"+data.message+"', "+rows[0].id+", '"+data.date_message+"')", function(err, rows, fields){
            });
            staff.ticket = true;
            staff.ticket_id = rows[0].id;
          });
        }else {
          connection.query("insert into `messages` (id_emissor_staff, content, id_ticked, time_message) values ("+data.id_db+", '"+data.message+"', "+staff.ticket_id+", '"+data.date_message+"')", function(err, rows, fields) {

          });
        }
        for(var i = 0;i<users.length;i++){
          if(users[i].id_db == data.to){
            io.to(users[i].id).emit('new_message', data);
          }
        }
        // comprobar si el ticket al que vas a contestar tiene id_staff
        // si no lo tiene, pongo el mio
        // si lo tiene y no soy yo, pues se avisa
        // si lo tiene y soy yo, pues se envia
        // Hay que hacerselo llegar al staff y a el usuario
        break;
    }
  });
  socket.on('del-chat', function(data){
    for(var i = 0;i<staff.length;i++){
      if(staff[i].id_db == data.to){
        staff[i].ticket = false;
        staff[i].ticket_id = null;
      }
      console.log(staff[i]);
    }
    for(var i = 0;i<users.length;i++){
      if(users[i].id_db == data.to){
        users[i].ticket = false;
        users[i].ticket_id = null;
        io.to(users[i].id).emit('del-chat');
      }
      console.log(users[i]);
    }
  });

  socket.on('my_id', function(data) {
    switch(data.group) {
      case 'user':
        user.id_db = data.id;
        var index = _.findIndex(users, function(o) { user.id === o.id })
        users[index] = user;
        console.log(users[index]);
        break;
      case 'staff':
        user.id_db = data.id;
        break;
    }
  });

  socket.on('answer', function(data) {
    switch(data.group) {
      case 'staff':
        io.to('/#'+data.to).emit('answer', { from: data.id, message: data.message, group: data.group });
        break;
      case 'user':
        break;
    }
  });

  socket.on('disconnect', function(data) {
    if(user.group === 'staff') {
      var index = _.findIndex(staff, function(o) {
        return o.id === socket.id;
      });
      staff.splice(index, 1);
      io.emit('staff_disconnect', { staffmembers: staff.length });
    }else {
      var index = _.findIndex(users, function(o) {
        return o.id === socket.id;
      });
      users.splice(index, 1);
    }
  });
});

//Toda la logica


app.listen(3000);
console.log(Date()+'>>'+'WebSockets Server listening on 3000');
