<?php
require_once __DIR__ . '/../app/Model.php';
require_once __DIR__ . '/../app/Config.php';
if(isset($_POST['id'])){
  $m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
      Config::$mvc_bd_clave,Config::$mvc_bd_hostname);
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $r = $m->getRecentConversations($_POST['id']);
    if($r == -1){
      $result['code']='ERR';
      $result['data']='INTERNAL_ERROR';
      echo json_encode($result);
    }else{
      $result['code']='SUCCESS';
      $result['data']=$r;
      echo json_encode($result);
    }
  }
}
?>
