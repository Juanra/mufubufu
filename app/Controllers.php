<?php
class Controller{
	public function landing() {
		session_start();
		require __DIR__ . '/templates/landing.php';
	}

	public function support() {
		session_start();
		require __DIR__ . '/templates/support.php';
	}

	public function logSupport() {
		session_start();
		$params = array( 'user' =>'','password' =>'');
		$m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
				Config::$mvc_bd_clave,Config::$mvc_bd_hostname);

		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			if($m->validateData($_POST['user'], $_POST['password'])){
				if($m->logUser($_POST['user'],$_POST['password'])!=0){
					$res = $m->logUser($_POST['user'],$_POST['password']);
					setcookie('usrid',$res['id']);
					header('Location:index.php?ctl=contactsupport');
				}else{
					$params['error'] = "Error al loguearte, el usuario no existe.";
				}
			}else{
				$params = array('user' => $_POST['user'],
						'password' => $_POST['password'],
				);
				$params['error'] = "Error al loguearte, compruebe usuario y contraseña.";
			}
		}
		require __DIR__ . '/templates/logsupport.php';
	}

	public function logstaff() {
		session_start();
		$params = array( 'user' =>'','password' =>'');
		$m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
				Config::$mvc_bd_clave,Config::$mvc_bd_hostname);

		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			if($m->logstaff($_POST['user'],$_POST['password'])!=0){
				$res = $m->logstaff($_POST['user'],$_POST['password']);
				setcookie('usrid',$res['id']);
				header('Location:index.php?ctl=staffroom&group=staff');
			}else{
				$params['error'] = "Error al loguearte, el usuario no existe.";
			}
		}
		require __DIR__ . '/templates/logstaff.php';
	}

	public function contactsupport() {
		session_start();
		if(!isset($_COOKIE['usrid']) && empty($_COOKIE['usrid'])){
			header('Location:index.php?ctl=landing');
		}else{
			$params = array('email' =>'', 'subject' =>'', 'message'=> '');
			$m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
					Config::$mvc_bd_clave,Config::$mvc_bd_hostname);
					if($_SERVER['REQUEST_METHOD'] == 'POST'){

					}
		}
		require __DIR__ . '/templates/contactsupport.php';
	}

	public function regsupport() {
		session_start();
		$params = array( 'user' =>'','password' =>'','email' =>'');
		$m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
				Config::$mvc_bd_clave,Config::$mvc_bd_hostname);
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			if($m->validateData($_POST['user'], $_POST['password'], $_POST['email'])){
				$clave = $m->crypt_blowfish($_POST['password']);
				if($m->insertUser($_POST['user'], $clave, $_POST['email'])){
					$_SESSION['user'] = $_POST['user'];
					header('Location:index.php?ctl=contactsupport');
				}else{
					$params['error'] = 'Error, el usuario ya existe.';
				}
			}else{
				$params = array('user' => $_POST['user'],
												'password' => $_POST['password'],
												'email' => $_POST['email']
				);
				$params['error'] = 'Error al registrar usuario. Porfavor revisa el formulario';
			}
		}
		require __DIR__ . '/templates/regsupport.php';
	}

	public function userroom() {
		$m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
		Config::$mvc_bd_clave,Config::$mvc_bd_hostname);
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
		    $r = $m->getUser($_POST['id']);
		    echo json_encode($r);
		}else if($_SERVER['REQUEST_METHOD'] == 'DELETE'){
			$r = $m->deleteConversation($_GET['id'], $_GET['group']);
			if($r == -1){
				return -1;
			}else{
				return 1;
			}
		}else {
			require __DIR__ . '/templates/userroom.php';
		}
	}

	public function staffroom() {
		require __DIR__ . '/templates/staffroom.php';
	}

	public function panel(){
		session_start();
		$usuario = $_SESSION['usuario'];
		$m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
				Config::$mvc_bd_clave,Config::$mvc_bd_hostname);
		if(isset($_GET['eliminar'])){
			if(!$m->eliminaAlbum($_GET['eliminar']))
				$alerta['mensaje'] = "No se ha podido eliminar el album.";
		}

		$foto = $m->dameFotoPerfil($usuario);

		$params = array('foto' => "../web/images/" . $foto,
				'albumes' => $m->dameAlbumes($usuario),
				'amigos' => $m->dameAmigos($usuario),
		);

		require __DIR__ . '/templates/mipanel.php';
	}

	public function editarperfil(){
		session_start();
		$usuario = $_SESSION['usuario'];
		$m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
				Config::$mvc_bd_clave,Config::$mvc_bd_hostname);
		$foto = $m->dameFotoPerfil($usuario);

		$params = array('foto' => "../web/images/" . $foto,
		);

		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			if($m->editaPerfil($usuario, $_FILES)){
				header('Location:index.php?ctl=panel');
			}else{
				$params['mensaje'] = "Error al modificar la foto de tu perfil.";
			}
		}

		require '/templates/editarperfil.php';
	}

	public function mialbum(){
		session_start();
		$usuario = $_SESSION['usuario'];
		$m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
				Config::$mvc_bd_clave,Config::$mvc_bd_hostname);

		$foto = $m->dameFotoPerfil($usuario);

		$params = array('foto' => "../web/images/" . $foto,
		);

		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			if($m->registraAlbum($usuario,$_POST['nombreAlbum'],$_POST['descripcion'],$_POST['visibilidad'])){
				$m->anyadePortadaAlbum($_POST['nombreAlbum'],$_FILES);
				if($m->registraFotos($usuario, $_FILES, $_POST['nombreAlbum'])){
 					header("refresh:3; url=index.php?ctl=panel");
				}else{
					$params['mensaje'] = "Ups! parece que no se han podido subir las fotos.";
				}
			}else{
				$params['mensaje'] = "Ups! Error al crear el álbum. ¿Existe ya un álbum con el mismo nombre?";
			}
			//print(count($_FILES['img']['name']));
		}

		require __DIR__ . '/templates/mialbum.php';
	}

	public function editaralbum(){
		session_start();
		$usuario = $_SESSION['usuario'];
		$m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
				Config::$mvc_bd_clave,Config::$mvc_bd_hostname);

		if(isset($_GET['editar'])){
			$_SESSION['album'] = $m->dameAlbum($_GET['editar']);
			$album = $_SESSION['album'];
		}

		$foto = $m->dameFotoPerfil($usuario);

		$params = array('foto' => "../web/images/" . $foto,
		);

		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			if($m->editaAlbum($_POST['nombreAlbum'], $_POST['descripcion'], $_POST['visibilidad'], $_SESSION['album']['nombre'])){
				header('Location:index.php?ctl=panel');
			}else{
				$params['mensaje'] = "Error al modificar el álbum.";
			}
		}

		require __DIR__ . '/templates/editaralbum.php';
	}

	public function misfotos(){
		session_start();
		$usuario = $_SESSION['usuario'];
		$m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
				Config::$mvc_bd_clave,Config::$mvc_bd_hostname);
		if (isset($_GET['nombre'])) {
			$_SESSION['nombreAlbum'] = $_GET['nombre'];
		}
		if(isset($_GET['eliminar'])){
			if(!$m->eliminaFoto($_GET['eliminar']))
				$alerta['mensaje'] = "No se ha podido eliminar el album.";
		}
		if(isset($_GET['nick_usu'])){
			$usuario = $_GET['nick_usu'];
		}

		$foto = $m->dameFotoPerfil($usuario);

		$params = array('foto' => "../web/images/" . $foto,
				'fotosAlbum' => $m->dameFotosAlbum($usuario, $_SESSION['nombreAlbum']),
				'perfilUsu' => $usuario,
		);

		if($_SERVER['REQUEST_METHOD'] == "POST"){
			if($m->registraFotos($usuario, $_FILES, $_SESSION['nombreAlbum'])){
				header("Location:index.php?ctl=misfotos");
			}else{
				$params['mensaje'] = "Ups! Error al añadir fotos nuevas al álbum.";
			}
		}
		require __DIR__ . '/templates/misfotos.php';
	}

	public function verfoto(){
		session_start();
		$usuario = $_SESSION['usuario'];
		$m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
				Config::$mvc_bd_clave,Config::$mvc_bd_hostname);

		if (!isset($_GET['nombre'])) {
			throw new Exception('Página no encontrada');
		}

		$foto = $m->dameFotoPerfil($usuario);

		$params = array('foto' => "../web/images/" . $foto,
				'fotoAlbum' => $_GET['nombre'],
		);

		require __DIR__ . '/templates/verfoto.php';
	}

	public function explorar(){
		session_start();
		$m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
				Config::$mvc_bd_clave,Config::$mvc_bd_hostname);

		if(!isset($_SESSION['usuario'])){
			$_SESSION['usuario'] = "invitado";
			$usuario = $_SESSION['usuario'];
		}else{
			$usuario = $_SESSION['usuario'];
			if($usuario != "invitado"){
				$foto = $m->dameFotoPerfil($usuario);
				$paramsFoto = "../web/images/" . $foto;
			}
		}
		$params = array('usuario' => $usuario,
				'fotosExplorar' => $m->explorarFotos($usuario, 2),
				'usuarios' => $m->recogeUsuarios($usuario),
		);

		require __DIR__ . '/templates/explorar.php';
	}

	public function verusuario(){
		session_start();
		$usuario = $_SESSION['usuario'];
		$m = new Model(Config::$mvc_bd_nombre,Config::$mvc_bd_usuario,
				Config::$mvc_bd_clave,Config::$mvc_bd_hostname);
		if (isset($_GET['nick'])) {
			//throw new Exception('Página no encontrada');
			$_SESSION['usuarioDestino'] = $_GET['nick'];
		}

		$foto = $m->dameFotoPerfil($usuario);

		$params = array('foto' => "../web/images/" . $foto,
				'albumPerfilUsu' => $m->muestraPerfilUsuario($usuario, $_SESSION['usuarioDestino']),
				'nombrePerfilUsu' => $_SESSION['usuarioDestino'],
				'perfilUsu' => $m->dameUsuario($_SESSION['usuarioDestino']),
		);

		if($m->somosAmigos($usuario,$_SESSION['usuarioDestino'])){
			$params['amigo'] = "si";
		}else{
			$params['amigo'] = "no";
		}

		if($_SERVER['REQUEST_METHOD'] == "POST"){
			if($_POST['amigo'] == 0){
				$m->eliminarAmigo($usuario, $_SESSION['usuarioDestino']);
				header('Location:index.php?ctl=verusuario');
			}else if($_POST['amigo'] == 1){
				$m->anyadiramigo($usuario, $_SESSION['usuarioDestino']);
				header('Location:index.php?ctl=verusuario');
			}
		}

		require __DIR__ . '/templates/verusuario.php';
	}

	public function loginTest(){
		$result=[];
		$result['data']='ok';
		echo json_encode($result);
		exit();
	}
}
?>
