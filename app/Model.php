<?php
class Model{
	protected $conexion;

	public function __construct($dbname,$dbuser,$dbpass,$dbhost) {
		$mvc_bd_conexion = mysql_connect($dbhost, $dbuser, $dbpass);

		if (!$mvc_bd_conexion) {
			die('No ha sido posible realizar la conexión con la base de datos: ' . mysql_error());
		}

		mysql_select_db($dbname, $mvc_bd_conexion);
		mysql_set_charset('utf8');
		$this->conexion = $mvc_bd_conexion;
	}

	public function bd_conexion() {

	}

	// Funcion para registrar usuario, pertecene a registro():
	public function insertUser ($n, $c, $e) {
		$sql = "insert into user (username,password,email) values ('$n','$c','$e')";
		$result = mysql_query($sql, $this->conexion);
		return $result;
	}

	//Funcion para loguearse, pertenece a landing():
	public function logUser($n, $c) {
		$sql = "select * from user where username='".$n."' OR email='".$n."'";
		$result = mysql_query($sql,$this->conexion);
		$fila = mysql_fetch_array($result);
		$pass = crypt($c,$fila['password']);
		if($pass == $fila['password']){
			return $fila;
		}else{
			return 0;
		}
	}

	public function logstaff($n, $c) {
		$sql = "select * from staff where name='".$n."' and password='".$c."'";
		$result = mysql_query($sql,$this->conexion);
		$fila = mysql_fetch_array($result);
		if($fila != false){
			return $fila;
		}else{
			return 0;
		}
	}

	public function getUser($id){
		$sql = "select * from user where id = ".$id."";
		$result = mysql_query($sql,$this->conexion);
		$affected = mysql_fetch_array($result);
		if($affected != false) {
			return $affected;
		}else {
			return -1;
		}
	}

	//Funcion de ayuda para validar los datos del registro, pertecene a registro():
	public function validateData($n, $c, $e="") {
		$reg = "/^[a-zA-Z0-9]{1,10}+$/";
		$n = strip_tags($n);
		$c = strip_tags($c);
		$e = strip_tags($e);
		$passEmail = 0;
		if($e!==""){
			if(filter_var($e,FILTER_VALIDATE_EMAIL)){
				$passEmail = 1;
			}
			return (preg_match($reg, $n) &
		preg_match($reg, $c) & $passEmail);
		}else{
			if(strlen($n)>=10){
				if(filter_var($n,FILTER_VALIDATE_EMAIL)){
					$passEmail = 1;
				}
				return (preg_match($reg, $c) & $passEmail);
			}else{
				return (preg_match($reg, $n) &
						preg_match($reg, $c));
			}
		}
	}

	//Funcion para encriptar contraseña, pertenece a registro():
	public function crypt_blowfish($password, $digito=8) {
		$set_salt = './1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		$salt = sprintf('$2a$%02d$', $digito);
		for($i = 0; $i < 22; $i++) {
			$salt .= $set_salt[mt_rand(0, 63)];
		}
		$pass= crypt($password, $salt);
		return $pass;
	}

	public function deleteConversation($id, $group){
		if($group == "user"){
			$sql = "delete from `ticket` where id_user=$id";
		}else{
			$sql = "delete from `ticket` where id_staff=$id";
		}
		$result = mysql_query($sql,$this->conexion);
		$afectadas = mysql_affected_rows();
		if($afectadas != -1){
			return 1;
		}else{
			return -1;
		}
	}

	public function getRecentConversations($id) {
		$sql = "select M.id,M.id_emissor_staff,M.id_emissor_user,M.content,M.id_ticked,M.time_message
		from messages M
		inner join ticket T on M.id_ticked = T.id where T.id_user = $id";
		$result = mysql_query($sql,$this->conexion);
		$affected = mysql_affected_rows();
		if($affected > 0) {
			while($row=mysql_fetch_array($result, MYSQLI_ASSOC)) {
				$res[]=$row;
			}
			return $res;
		}else {
			return -1;
		}
	}

}
?>
