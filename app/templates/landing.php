<?php ob_start()?>
  <script src="js/landing.js"></script>
<?php $loadScripts = ob_get_clean() ?>
<div class="video">
  <div class="video-hover">
    <i class="material-icons">play_circle_filled</i>
    <div class="description-video">
      <span>Don Fisher</span>
    </div>
  </div>
  <img src="img/don_fisher.png">
</div>
<div class="video">
  <div class="video-hover">
    <i class="material-icons">play_circle_filled</i>
    <div class="description-video">
      <span>Algo muy normal</span>
    </div>
  </div>
  <img src="img/something_normal.png">
</div>
<div class="video">
  <div class="video-hover">
    <i class="material-icons">play_circle_filled</i>
    <div class="description-video">
      <span>Languing App</span>
    </div>
  </div>
  <img src="img/languing.png">
</div>
<div class="video">
  <div class="video-hover">
    <i class="material-icons">play_circle_filled</i>
    <div class="description-video">
      <span>Intergalactic race</span>
    </div>
  </div>
  <img src="img/intergalactic_race.png">
</div>
<?php $content = ob_get_clean()?>
<?php include 'layoutPanel.php' ?>
