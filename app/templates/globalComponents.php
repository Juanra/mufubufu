<?php ob_start()?>
<div class="sticky-container">
  <ul class="sticky">
    <li class="fb-share-sidebar">
      <!-- <a class="a-fb"> -->
      <div class="div-icon-social div-facebook"><i  class="fa fa-facebook opacity-85"></i></div>
      <p>Share</p>
      <!-- </a> -->
    </li>
    <li class="tw-share-sidebar">
      <div class="div-icon-social div-twitter"><i  class="fa fa-twitter opacity-85"></i></div>
      <p>Share</p>
    </li>
    <!-- <li>
      <img width="32" height="32" title="" alt="" src="img/pin1.png" />
      <p>Pinterest</p>
    </li>
    <li>
      <img width="32" height="32" title="" alt="" src="img/li1.png" />
      <p>Linkedin</p>
    </li>-->
    <li class="contact-sidebar margin-top-50px">
      <div class="div-icon-social div-contact"><i  class="fa fa-envelope opacity-85"></i></div>
      <p>Contact</p>
    </li>
    <!--<li>
      <img width="32" height="32" title="" alt="" src="img/tm1.png" />
      <p>Tumblr</p>
    </li>
    <li>
      <img width="32" height="32" title="" alt="" src="img/wp1.png" />
      <p>WordPress</p>
    </li>
    <li>
      <img width="30" height="32" title="" alt="" src="img/vm1.png" />
      <p>Vimeo</p>
    </li> -->
  </ul>
</div>
<?php $global = ob_get_clean()?>
<?php include 'layoutPanel.php' ?>
