<?php ob_start()?>
  <script src="js/support.js"></script>
<?php $loadScripts = ob_get_clean() ?>
  <h1>Bienvenido al soporte técnico</h1>
  <!-- Modificar con router para enviar directamente a InicioSoporte
      o si existe la cookie a comenzarSoporte-->
  <a href="index.php?ctl=logsupport">Contactar con el soporte en linea</a>
<?php $content = ob_get_clean()?>
<?php include 'layoutPanel.php' ?>
