<?php ob_start()?>
  <script src="js/contactsupport.js"></script>
<?php $loadScripts = ob_get_clean() ?>
  <div class="content-contact-support">
    <h1>Soporte Técnico Online</h1>
    <h4>Bienvenido usuario</h4>
    <div class="content-of-chat">
      <div class="">
        <p class="message-contact-text" style="text-align:center">Dispone de nuestro Soporte Técnico de 8:00 a 20:00 horas, los 365 días del año incluidos festivos.</p>
      </div>
      <p class="online-offline-text" style="text-align:center">En estos momentos nuestros técnicos se encuentran en línea, entre para hacer su consulta.</p>
      <a class="enter-room" href="index.php?ctl=userroom&group=user"></a>
      <div class="btn-enter-room">
        <span class="span-enter-room">Enter Room</span>
      </div>
      <div class="offline-contact">
        <span>Submit request</span>
      </div>
    </div>
    <div class="content-of-offline-contact hidden">
      <h4>Submit a request</h4>
      <form class="" action="index.php?ctl=contactsupport" method="post">
        <span class="title-offline-contact">Tu correo electrónico <span class="red">*</span></span>
        <input type="text" name="email" class="email-text" value="<?php ?>"/>
        <span class="error-email red"></span>
        <span class="title-offline-contact">Asunto <span class="red">*</span></span>
        <input type="text" name="subject" class="subject-text" value="<?php ?>"/>
        <span class="error-subject red"></span>
        <span class="title-offline-contact">Mensaje <span class="red">*</span></span>
        <textarea name="message" class="textarea-message-text" cols=45 rows=8 value="<?php ?>"></textarea>
        <span class="error-message red"></span>
        <input type="submit" class="offline-contact-submit" name="offline-contact" value="Enviar"/>
      </form>
    </div>
  </div>
<?php $content = ob_get_clean()?>
<?php include 'layoutPanel.php' ?>
