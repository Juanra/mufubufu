<?php ob_start()?>
  <script src="js/logsupport.js"></script>
<?php $loadScripts = ob_get_clean() ?>
  <h1>Contactar con el soporte técnico</h1>
  <h4>Inicia sesión para obtener soporte técnico</h4>
  <div class="content-support">
    <form class="" action="index.php?ctl=logstaff" method="post">
      <input type="text" name="user" class="user-text-log" value="<?php echo $params['user']?>" placeholder="Usuario o Email" data-toggle="popover" autofocus/>
      <input type="password" name="password" class="password-text-log" value="<?php echo $params['password']?>" placeholder="Contraseña" data-toggle="popover"/>
      <input type="submit" class="log-support disabled" name="log" value="Login" disabled/>
    </form>
  </div>
<?php if(isset($params['error'])): ?>
  <span><?php echo $params['error'] ?></span>
<?php endif; ?>
<?php $content = ob_get_clean()?>
<?php include 'layoutPanel.php' ?>
