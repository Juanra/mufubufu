<?php ob_start()?>
  <script src="js/regsupport.js"></script>
<?php $loadScripts = ob_get_clean() ?>
  <h3>Register Support</h3>
  <h4>Registrate para recibir soporte técnico</h4>
  <div class="content-support">
    <form class="" action="index.php?ctl=regsupport" method="post">
      <input type="text" name="user" class="user-text-reg" value="<?php echo $params['user']?>" placeholder="Usuario" data-toggle="popover" autofocus/>
      <input type="password" name="password" class="password-text-reg" value="<?php echo $params['password']?>" placeholder="Contraseña" data-toggle="popover"/>
      <input type="password" name="rep-password" class="rep-password-text-reg" value="" placeholder="Repite contraseña" data-toggle="popover"/>
      <input type="text" name="email" class="email-text-reg" value="<?php echo $params['email']?>" placeholder="Email" data-toggle="popover"/>
      <input type="submit" class="reg-support disabled" name="log" value="Registrarse" disabled/>
    </form>
  </div>
<?php if(isset($params['error'])): ?>
  <span><?php echo $params['error'] ?></span>
<?php endif; ?>
<?php $content = ob_get_clean()?>
<?php include 'layoutPanel.php' ?>
