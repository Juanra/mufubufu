<?php ob_start()?>
  <script src="js/client.js"></script>
  <script src="js/staffroom.js"></script>
<?php $loadScripts = ob_get_clean() ?>
  <div class="content-staff-room">
    <div class="content-recent-conversations">

    </div>
    <div class="content-conversation">
      <div class="information-header-chat">

      </div>
      <div class="colum-chat">
    <!--    <div class="show-indication">
          <span>Envia tu consulta, en breves te atenderá nuestro equipo.</span>
        </div> -->
        <div class="col-xs-12 content-messages-chat">
        </div>
      </div>
      <div class="content-chat">
        <textarea class="input-chat form-control" autoresize="true" rows="1" max-rows="8" placeholder="Escribe tu mensaje aquí">
        </textarea>
        <div class="btn-send">
          <span class="material-icons black-t04 send-message btn-send-color">
            send
          </span>
        </div>
      </div>
    </div>
  </div>
<?php $content = ob_get_clean()?>
<?php include 'layoutPanel.php' ?>
