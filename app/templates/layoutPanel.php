<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Mufubufu</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php echo 'css/'.Config::$mvc_vis_css ?>" />
    <link rel="stylesheet" href="css/jquery.fullPage.css"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Comfortaa:300&subset=latin,cyrillic-ext,greek,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700&subset=latin,greek,greek-ext,vietnamese,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/jquery.i18n.min.js"></script>
    <script src="https://cdn.jsdelivr.net/lodash/4.6.1/lodash.min.js"></script>

  <!--  <script src="/assets/js/typeahead/bootstrap-typeahead.min.js"></script>
    <script src="/assets/js/jquery.cookie.js"></script>
    <script src="/assets/js/jquery.cookiecuttr.js"></script> -->
    <script src="js/jquery.fullPage.min.js"></script>
    <script src="js/dropdowns-enhancement.js"></script>
  <!--  <script src="/assets/js/ripples.min.js"></script>
    <script src="/assets/js/material.min.js"></script> -->
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://cdn.socket.io/socket.io-1.3.7.js"></script>
    <script src="/assets/js/pace.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
    <script src="js/layoutpanel.js"></script>
		<?php echo $loadScripts ?>

  </head>
  <body>
    <div class="header">
    	<img src="img/portada.jpg" alt="mufubufu_portada"/>
    </div>
    <div class="content">
      <div class="sticky-container">
        <ul class="sticky">
          <li class="fb-share-sidebar">
            <!-- <a class="a-fb"> -->
            <div class="div-icon-social div-facebook"><i  class="fa fa-facebook opacity-85"></i></div>
            <p>Share</p>
            <!-- </a> -->
          </li>
          <li class="tw-share-sidebar">
            <div class="div-icon-social div-twitter"><i  class="fa fa-twitter opacity-85"></i></div>
            <p>Share</p>
          </li>
          <!-- <li>
            <img width="32" height="32" title="" alt="" src="img/pin1.png" />
            <p>Pinterest</p>
          </li>
          <li>
            <img width="32" height="32" title="" alt="" src="img/li1.png" />
            <p>Linkedin</p>
          </li>-->
          <li class="contact-sidebar margin-top-50px">
            <div class="div-icon-social div-contact"><i  class="fa fa-envelope opacity-85"></i></div>
            <p>Contact</p>
          </li>
          <!--<li>
            <img width="32" height="32" title="" alt="" src="img/tm1.png" />
            <p>Tumblr</p>
          </li>
          <li>
            <img width="32" height="32" title="" alt="" src="img/wp1.png" />
            <p>WordPress</p>
          </li>
          <li>
            <img width="30" height="32" title="" alt="" src="img/vm1.png" />
            <p>Vimeo</p>
          </li> -->
        </ul>
      </div>

			<?php echo $content ?>
		</div>
	</body>
</html>
